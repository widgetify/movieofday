const fetch = require('node-fetch')
const express = require('express')
var request = require("request")
const router = express.Router()

const API_URL = "https://api.themoviedb.org/3/discover/movie?api_key=fa5580f363398295f54eba84b307c81e&vote_average.gte=7&with_original_language=en&primary_release_year=";
const PAGINATION = '&page='




router.get('/', async (req, res, next) => {
  let loop = true
  while(loop){
    try {
        let randomYear = 1990 + Math.floor(Math.random() * (2020-1980))
        let randomPage = Math.floor(Math.random() * 15) + 1
        let randomNumber = Math.floor(Math.random() * 19) + 1
        let url = API_URL + randomYear.toString() + PAGINATION + randomPage
        const getData = async url => {
          try {
            const response = await fetch(url);
            const json = await response.json();
            console.log(url)
            if (json.results[randomNumber].poster_path){
              loop = false
              res.json(json.results[randomNumber])
            }
          } catch (error) {
            console.log(error);
          }
        }
        
        await getData(url);   
        
        } catch (error) {
          res.json({ error: error.message })
        }
        }
      }
    )

  module.exports = router